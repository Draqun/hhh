#ifndef BEE_H
#define BEE_H

#include <Movable.hpp>

class Bee: public Movable
{
// Public Methods
public:
	using Movable::Movable;
	virtual ~Bee();
};
#endif // BEE_H
