#pragma once

#include <Movable.hpp>

class Cloud : public Movable
{
// Public Methods
public:
	using Movable::Movable;
	virtual ~Cloud();
};

