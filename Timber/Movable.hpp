#pragma once

#include <limits>
#include <random>
#include <type_traits>

#include <SFML/Graphics.hpp>

using speed_distribution = std::uniform_real_distribution<float_t>;
using height_distribution = std::uniform_real_distribution<float_t>;

class MovableError : std::exception
{};

class Movable
{
// Private Members
	float_t horizontal_default_position;

// Protected Members
protected:
	std::shared_ptr<sf::Texture> texture;
	sf::Sprite sprite;

// Public Members
public:
	bool is_active;
	float speed;

// Public Methods
public:
	Movable();
	Movable(const Movable &right);
	Movable(Movable &&right);
	Movable(const float_t horizontal_default_position);
	Movable(const float_t horizontal_default_position, sf::Texture *texture);
	Movable(const float_t horizontal_default_position, const std::string &texture_file);
	virtual ~Movable();
	void run() noexcept;
	void stop() noexcept;
	void set_texture(sf::Texture &texture) noexcept;

	sf::Sprite* operator->() { return &sprite; }
	Movable& operator=(const Movable &right) noexcept;
	Movable& operator=(Movable &&right) noexcept;
	const sf::Sprite& get_sprite() const { return sprite; }

	template<uint16_t lowest_speed, uint16_t highest_speed>
	void set_speed(std::default_random_engine &dre) { speed_distribution speed_dist(lowest_speed, highest_speed); speed = speed_dist(dre); }

	template<uint16_t lowest_height, uint16_t highest_height>
	void set_height(std::default_random_engine &dre)
	{
		height_distribution heigh_dist(lowest_height, highest_height);
		sprite.setPosition(horizontal_default_position, heigh_dist(dre));
	}

	const unsigned int get_texture_width();
	const unsigned int get_texture_height();

};

