#include <vector>
#include <random>

#include <SFML/Graphics.hpp>

#include <Bee.hpp>
#include <Cloud.hpp>

const uint16_t SCREEN_WIDTH{ 1920 };
const uint16_t SCREEN_HEIGH{ 1080 };

int main()
{
	sf::VideoMode vm{ SCREEN_WIDTH, SCREEN_HEIGH };

	sf::RenderWindow rw{ vm, "Timber", sf::Style::Fullscreen };
	sf::Texture bg{};
	bg.loadFromFile("graphics/background.png");

	sf::Sprite bg_sprite{};
	bg_sprite.setTexture(bg);
	bg_sprite.setPosition(0, 0);

	sf::Texture tree{};
	tree.loadFromFile("graphics/tree.png");

	sf::Sprite tree_sprite{};
	tree_sprite.setTexture(tree);
	tree_sprite.setPosition(810, 0);

	std::vector<Bee> bees(1, { 2000.0f, "graphics/bee.png" });

	std::vector<Cloud> clouds(3, { -300.0f });
	{
		sf::Texture cloud_texture{};
		cloud_texture.loadFromFile("graphics/cloud.png");
		for (auto &cloud : clouds)
		{
			cloud.set_texture(cloud_texture);
		}
	}

	std::random_device rd{};
	std::default_random_engine dre{ rd() };

	sf::Clock clock{};
	sf::Time dt{};
	while (rw.isOpen())
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
		{
			rw.close();
		}

		rw.clear();

		dt = clock.restart();
		for (auto &bee : bees)
		{
			if (!bee.is_active)
			{
				bee.set_speed<199, 399>(dre);
				bee.set_height<499, 999>(dre);
				bee.run();
			}
			else
			{
				bee->setPosition(
					bee->getPosition().x - (bee.speed * dt.asSeconds()),
					bee->getPosition().y
				);
				if (bee->getPosition().x < -static_cast<int>(bee.get_texture_width()))
				{
					bee.stop();
				}
			}
		}

		for (auto &cloud : clouds)
		{
			if (!cloud.is_active)
			{
				cloud.set_speed<199, 399>(dre);
				cloud.set_height<0, 149>(dre);
				cloud.run();
			}
			else
			{
				cloud->setPosition(
					cloud->getPosition().x + (cloud.speed * dt.asSeconds()),
					cloud->getPosition().y
				);
				if (cloud->getPosition().x > SCREEN_WIDTH)
				{
					cloud.stop();
				}
			}
		}
		
		rw.draw(bg_sprite);
		for (auto &cloud : clouds)
		{
			rw.draw(cloud.get_sprite());
		}

		rw.draw(tree_sprite);

		for (auto &bee : bees)
		{
			rw.draw(bee.get_sprite());
		}
		rw.display();
	}

	return 0;
}