#include <Movable.hpp>

#include <iostream>

Movable::Movable() : sprite{}, texture{ new sf::Texture{} }, is_active{ false }, speed{ 0 },
horizontal_default_position{ 0 }
{
}

Movable::Movable(const Movable &right) : sprite{ right.sprite }, texture{ right.texture }, is_active{ right.is_active },
speed{ right.speed }, horizontal_default_position{ right.horizontal_default_position }
{
}

Movable::Movable(Movable &&right) : sprite{ right.sprite }, texture{ std::move(right.texture) },
is_active{ right.is_active }, speed{ right.speed }, horizontal_default_position{ right.horizontal_default_position }
{
}

Movable::Movable(const float_t horizontal_default_position) : sprite{}, texture{ new sf::Texture{} },
is_active{ false }, speed{ 0 }, horizontal_default_position{ horizontal_default_position }
{
}

Movable::Movable(const float_t horizontal_default_position, sf::Texture *texture) : sprite{},
texture{ std::make_shared<sf::Texture>(*texture) }, is_active{ false }, speed{ 0 },
horizontal_default_position{ horizontal_default_position }
{
	this->sprite.setTexture(*this->texture);
}

Movable::Movable(const float_t horizontal_default_position, const std::string &texture_file) : sprite{},
texture{std::make_shared<sf::Texture>()}, is_active{ false }, speed{ 0 },
horizontal_default_position{ horizontal_default_position }
{
	if (!this->texture->loadFromFile(texture_file))
	{
		throw MovableError();
	}
	this->sprite.setTexture(*this->texture);
}

Movable::~Movable()
{
}

Movable& Movable::operator=(const Movable &right) noexcept
{
	this->sprite = right.sprite;
	this->texture = right.texture;
	this->is_active = right.is_active;
	this->speed = right.speed;
	this->horizontal_default_position = right.horizontal_default_position;
	return *this;
}

Movable& Movable::operator=(Movable &&right) noexcept
{
	this->sprite = right.sprite;
	this->texture = std::move(right.texture);
	this->is_active = right.is_active;
	this->speed = right.speed;
	this->horizontal_default_position = right.horizontal_default_position;
	return *this;
}

void Movable::run() noexcept
{
	this->is_active = true;
}

void Movable::stop() noexcept
{
	this->is_active = false;
}

void Movable::set_texture(sf::Texture &texture) noexcept
{
	this->texture = std::make_shared<sf::Texture>(texture);
	this->sprite.setTexture(*this->texture);
}

const unsigned int Movable::get_texture_width()
{
	return this->texture->getSize().x;
}

const unsigned int Movable::get_texture_height()
{
	return this->texture->getSize().y;
}